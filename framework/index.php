<?php

$loader = include('vendor/autoload.php');
$loader->add('', 'src');

$app = new Silex\Application;
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

$app['model'] = new Sondage\Model(
    'localhost',  // Hôte
    'sondage',    // Base de données
    'root',    // Utilisateur
    ''     // Mot de passe
);

// Page d'accueil
$app->match('/', function() use ($app) {
    $app['session']->set('page_courante','home');
    return $app['twig']->render('home.html.twig');
})->bind('home');

// Inscription
$app->match('/inscription', function() use ($app) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['login']) && isset($_POST['password'])) {
            return $app['twig']->render('inscription.html.twig', array(
                'inscription' =>$app['model']->sign($_POST['login'],$_POST['password'])
            ));
        }
    }
    else
    {
        return $app['twig']->render('inscription.html.twig', array(
            'inscription' => 0.1
        ));
    }
})->bind('inscription');

// Identification
$app->match('/identification', function() use ($app) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['login']) && isset($_POST['password'])) {
            return $app['twig']->render('identification.html.twig', array(
                'identification' => $app['model']->login($_POST['login'],$_POST['password']),
                'session' => $app['session']->set('user',$_SESSION['user'])
            ));
        }
    }
    else
    {
        return $app['twig']->render('identification.html.twig');
    }
})->bind('identification');

// liste des Sondage
$app->match('/sondages', function() use ($app) {

    return $app['twig']->render('sondages.html.twig', array(
        'questions' => $app['model']->listeSondage()
    ));
})->bind('sondages');

// Un sondage
$app->match('/sondage/{id}', function($id) use ($app) {

if  ($_SERVER['REQUEST_METHOD']=='POST' && !empty($_POST['answer']) &&
            ($_POST['answer']=='1' || $_POST['answer']=='2' || $_POST['answer']=='3')) 
{
    $app['session']->set('userAnswer'.$id,true);
    return $app['twig']->render('sondage.html.twig', array(
        'sondage' => $app['model']->afficher($id),
        'insert' => $app['model']->insertAnswer($id,$app['session']->get('user')['id'],$_POST['answer']),
        'answer' => $app['model']->getAnswer($id),
        'nbAnswer' => $app['model']->getTotalAnswer($id),
        'participate' => $app['model']->participate($id,$app['session']->get('user')['id'])
    ));
}
else
{
    return $app['twig']->render('sondage.html.twig', array(
        'sondage' => $app['model']->afficher($id),
        'answer' => $app['model']->getAnswer($id),
        'nbAnswer' => $app['model']->getTotalAnswer($id),
        'participate' => $app['model']->participate($id,$app['session']->get('user')['id'])
    ));
}
})->bind('sondage');


// Créer un sondage
$app->match('/nouveau_sondage', function() use ($app) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['question']) && isset($_POST['answer1']) && isset($_POST['answer2']) && isset($_POST['answer3'])) {
            return $app['twig']->render('nouveau_sondage.html.twig', array(
                'create' => $app['model']->createSondage($_POST['question'],$_POST['answer1'],$_POST['answer1'],$_POST['answer3'],$app['session']->get('user')['id'])
            ));
        }
    }
    else
    {
        return $app['twig']->render('nouveau_sondage.html.twig');
    }
})->bind('nouveau_sondage');

// Page d'accueil
$app->match('/deconnexion', function() use ($app) {
    $app['session']->remove('user');
    return $app['twig']->render('deconnexion.html.twig');
})->bind('deconnexion');

// Fait remonter les erreurs
$app->error(function($error) {
    throw $error;
});

$app->run();
