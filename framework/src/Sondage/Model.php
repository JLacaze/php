<?php

namespace Sondage;

/**
 * Représente le "Model", c'est à dire l'accès à la base de
 * données pour l'application cinéma basé sur MySQL
 */
class Model
{
    protected $pdo;

    public function __construct($host, $database, $user, $password)
    {
        try {
            $this->pdo = new \PDO(
                'mysql:dbname='.$database.';host='.$host,
                $user,
                $password
            );
            $this->pdo->exec('SET CHARSET UTF8');
        } catch (\PDOException $exception) {
            die('Impossible de se connecter au serveur MySQL');
        }
    }

    /**
     * Récupère un résultat exactement
     */
    protected function fetchOne(\PDOStatement $query)
    {
        if ($query->rowCount() != 1) {
            return false;
        } else {
            return $query->fetch();
        }
    }

    /**
    * Inscription
    */
    public function sign($login,$password)
    {
        $login = htmlspecialchars($login);
        $sql = 'SELECT COUNT(*) as nb FROM users WHERE login=?';
        $query = $this->pdo->prepare($sql);
        $query->execute(array($login));
        $req = $query->fetch();
        if ($req['nb'] == 0)
        {
            $password = htmlspecialchars($password);
            $query = $this->pdo->prepare('INSERT INTO users (login, password) VALUES (?,?)');
            $password = md5($password);
            $query->execute(array($login, $password));

            return true;
        } 
        else
        {
            return $login;
        }
    }

    /**
    * Identification
    * Si la fonction retourne le login : la connexion est un succès.
    * Si la fonction retourne autre chose : la connexion est un échec.
    */
    public function login($login,$password)
    {
        $login = htmlspecialchars($login);
        $sql = 'SELECT * FROM users WHERE login=?';
        $req = $this->pdo->prepare($sql);
        $req->execute(array($login));

        if ($req->rowCount()) {
            $user = $req->fetch();
            $password = htmlspecialchars($password);
            $password = md5($password);
            if ($user['password'] == $password) {
                $currentUser = $user;
                $_SESSION['user']['id'] = $currentUser['id'];
                $_SESSION['user']['login'] = $currentUser['login'];


                return $_SESSION['user'];
            }
        }
        else
        {
            return "password";
        }

        return $login;
    }

    /**
    * Sondage
    */
    public function listeSondage()
    {
        $sql = 'SELECT * FROM polls';
        $query = $this->pdo->query($sql);
        return $query;
    }

    public function afficher($id)
    {
        $id = htmlspecialchars($id);
        $sql = 'SELECT *, login FROM polls, users WHERE polls.user_id = users.id AND polls.id=?';
        $query = $this->pdo->prepare($sql);
        $query->execute(array($id));

        if ($query->rowCount()) {
            return $query->fetch();
        }
        return false;
    }
    /**
    * Participer Sondage
    */
    public function insertAnswer($poll_id,$user_id,$answer)
    {
        $sql = 'SELECT * FROM answers WHERE poll_id=? AND user_id=?';
        $query = $this->pdo->prepare($sql);
        $query->execute(array($poll_id,$user_id));
        if ($query->rowCount()) {
            $userAnswered = true;
            return true;
        } else {
            if (!empty($answer) &&
                ($answer=='1' || $answer=='2' || $answer=='3')) {
                    $answer = htmlspecialchars($answer);
                    $sql = 'INSERT INTO answers (user_id, poll_id, answer)
                            VALUES (?,?,?)';
                    $query = $this->pdo->prepare($sql);
                    $query->execute(array($user_id,$poll_id,$answer));
                    $userAnswered = true;
                    return true;
            }
            return false;
        }
    }

    public function getAnswer($id)
    {
        $answers = array();
        foreach (array(1,2,3) as $answer) {

            $sql = 'SELECT COUNT(*) as nb FROM answers WHERE 
            poll_id=? AND answer=?';
            $query = $this->pdo->prepare($sql);
            $query->execute(array($id,$answer));
            if ($query->rowCount()) {
                $query = $query->fetch();
                $answers[$answer] = $query['nb'];
            }
        }
        return $answers;
    }

    public function getTotalAnswer($id)
    {
        $sql = 'SELECT COUNT(*) as nb FROM answers WHERE poll_id=?';
        $req = $this->pdo->prepare($sql);
        $req->execute(array($id)); 
        if ($req->rowCount()) {
            return $req->fetch();
        }
        return false;
    }

    /**
    * Nouveau Sondage
    */
    public function createSondage($question,$answer1,$answer2,$answer3,$user_id)
    {
        $question = htmlspecialchars($question);
        $answer1 = htmlspecialchars($answer1);
        $answer2 = htmlspecialchars($answer2);
        $answer3 = htmlspecialchars($answer3);
        $sql = 'INSERT INTO polls (user_id,question,answer1,answer2,answer3)
                VALUES (?,?,?,?,?)';
        $query = $this->pdo->prepare($sql);
        $query->execute(array($user_id,$question,$answer1,$answer2,$answer3));

        return true;
    }

    /**
    * Deja participer
    */
    public function participate($poll_id,$user_id)
    {
        $sql = 'SELECT * FROM answers WHERE poll_id=? AND user_id=?';
        $query = $this->pdo->prepare($sql);
        $query->execute(array($poll_id,$user_id));
        if ($query->rowCount()) {
            return true;
        }
        return false;
    }

}